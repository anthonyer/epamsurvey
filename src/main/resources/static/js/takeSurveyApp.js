(function() {

	var app = angular.module("takeSurveyApp", []);

	var takeSurveyCtrl = function($scope, $http) {
		
		var questions = [];
		var answers = [];
		
		$scope.totalQuestions = 0;
		$scope.questionCount = 0;
		
		$scope.initialize = function() {
			if ($scope.surveyId == 0)
				return;
			
			$scope.taking = true;
		
			$http.get("/api/surveys/" + $scope.surveyId + "/questions?onlyValid=true")
				.then(
					function(response) {
						questions = questions.concat(response.data);
						$scope.totalQuestions = questions.length;
						$scope.setQuestion($scope.questionCount);
					}, 
					function(reason) {
						$scope.error = "Could not fetch the data.";
					}
				);
		};
		
		$scope.setQuestion = function(questionNumber) {

			$http.get("/api/questions/" + questions[questionNumber].id + "/answers")
				.then(
					function(response) {
						$scope.currentQuestion = questions[questionNumber];
						$scope.currentAnswers = response.data;
					}, 
					function(reason) {
						$scope.error = "Could not fetch the data.";
					}
				);
		};
		
		$scope.answerQuestion = function(selection) {
			if (selection === undefined) {
				alert("Please, choose an answer");
				return;
			}
						
			answers.push({
				question: $scope.currentQuestion.id,
				selectedAnswer: selection
			});
			
			$scope.questionCount++;
			if ($scope.questionCount == $scope.totalQuestions) {
				$scope.submitAnswers();
			} else {
				$scope.setQuestion($scope.questionCount);
			}	
		};
		
		$scope.submitAnswers = function() {
            $scope.taking = false;
		};
	
		$scope.initialize();	
	};

	app.controller("takeSurveyCtrl", ["$scope", "$http", takeSurveyCtrl]);

}());