(function() {

	var app = angular.module("homeApp", []);

	var homeCtrl = function($scope, $http) {	  
	
		$scope.pagination = {
			pageNumber: 0,
            pageSize: 5,
			morePagesAvailable: true
		};
		
		$scope.loadNextPage = function(surveyName, surveyDescription) {
		
			if ($scope.pagination.morePagesAvailable) {
				$http.get("/api/surveys?published=true&sort=id,desc&page=" + $scope.pagination.pageNumber + "&pageSize=" + $scope.pagination.pageSize)
					.then(
						function(response) {
							if ($scope.surveys == undefined) {
								$scope.surveys = response.data.content;
							} else {
								$scope.surveys = $scope.surveys.concat(response.data.content);
							}
							
							$scope.pagination.morePagesAvailable = !response.data.last;
							$scope.pagination.pageNumber++;
						}, 
						function(reason) {
							$scope.error = "Could not fetch the data.";
						}
					);
			}
		};
		$scope.loadNextPage();
	};

	app.controller("HomeCtrl", ["$scope", "$http", homeCtrl]);

}());