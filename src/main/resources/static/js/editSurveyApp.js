(function() {

	var app = angular.module("editSurveyApp", ['dndLists']);

	var editCtrl = function($scope, $http) {
		
		$scope.saving = false;
		$scope.publishing = false;
		
		$scope.refreshSurveyData = function() {
			if ($scope.surveyId == 0)
				return;

			$http.get("/api/surveys/" + $scope.surveyId)
			.then(
					function(response) {
						$scope.survey = response.data;
					}, 
					function(reason) {
						console.log(reason.data);
					}
			);
		};

		$scope.refreshQuestions = function() {
			if ($scope.surveyId == 0)
				return;

			$http.get("/api/surveys/" + $scope.surveyId + "/questions/")
			.then(
					function(response) {
						$scope.questions = response.data;
						$scope.newQuestion = "";
					}, 
					function(reason) {
						console.log(reason.data);
					}
			);
		};
		
		$scope.saveSurvey = function() {
			var url = "/api/surveys/";
			
			if ($scope.surveyId != 0)
				url = url + $scope.surveyId + "/";

			$http.post(url + "?name=" + $scope.survey.name + "&description=" + $scope.survey.description)
			.then(
					function(response) {
						console.log(response.data);
					}, 
					function(reason) {
						alert(reason.data);
						console.log(reason.data);
					}
			);
		};
		
		$scope.saveQuestion = function(questionId, questionText) {
			var url = "/api/questions";
			
			if (questionId != 0) {
				url = url + "/" + questionId;
			}

			$http.post(url + "?text=" + questionText + "&surveyId=" + $scope.surveyId)
			.then(
					function(response) {
						console.log(response.data);
						if (questionId == 0) {
							$scope.questions.push(response.data);
							$scope.newQuestion = "";
						}
					}, 
					function(reason) {
						console.log(reason.data);
					}
			);
		};
		
		$scope.saveAll = function() {
			var url = "/api/questions/updateAll";
			
			$scope.saving = true;
			$scope.saveSurvey();

			$http.post(url + "?surveyId=" + $scope.surveyId,
				JSON.stringify($scope.questions))
			.then(
					function(response) {
						console.log(response.data);
						$scope.saving = false;
					}, 
					function(reason) {
						alert(reason.data);
						console.log(reason.data);
						$scope.saving = false;
					}
			);
		};

		$scope.publish = function() {
			var url = "/api/surveys/" + $scope.surveyId + "/publish";
			
			$scope.publishing = true;
			$scope.saveAll();

			$http.post(url)
			.then(
					function(response) {
						console.log(response.data);
						$scope.survey.isPublished=false;
						$scope.publishing = false;
					}, 
					function(reason) {
						console.log(reason.data);
						alert("Please, set up at least one question with answers");
						$scope.publishing = false;
					}
			);
		};
		
		$scope.deleteQuestion = function(questionId) {
			if (questionId == 0)
				return;

			$http.delete("/api/questions/" + questionId)
			.then(
					function(response) {
						for (var i = 0; i < $scope.questions.length; i++) {
						    if ($scope.questions[i].id == questionId) {
						    	$scope.questions.splice(i, 1);
						    	break;
						    }
						}
					}, 
					function(reason) {
						console.log(reason.data);
					}
			);
		};
		
		$scope.refreshSurveyData();
		$scope.refreshQuestions();
	};

	app.controller("EditSurveyCtrl", ["$scope", "$http", editCtrl]);

}());