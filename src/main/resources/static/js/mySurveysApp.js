(function() {

	var app = angular.module("mySurveysApp", []);

	var mySurveysCtrl = function($scope, $http) {
	
		$scope.pagination = {
			pageNumber: 0,
            pageSize: 5,
			morePagesAvailable: true
		};
		
		$scope.loadNextPage = function(surveyName, surveyDescription) {
		
			if ($scope.pagination.morePagesAvailable) {
				$http.get("/api/surveys/" + $scope.userId + "/surveys?page=" + $scope.pagination.pageNumber + "&pageSize=" + $scope.pagination.pageSize)
					.then(
						function(response) {
							if ($scope.surveys == undefined) {
								$scope.surveys = response.data.content;
							} else {
								$scope.surveys = $scope.surveys.concat(response.data.content);
							}
							
							$scope.pagination.morePagesAvailable = !response.data.last;
							$scope.pagination.pageNumber++;
						}, 
						function(reason) {
							$scope.error = "Could not fetch the data.";
						}
					);
			}
		};
		
		$scope.deleteSurvey = function(surveyId) {
			if (surveyId == 0)
				return;

			$http.delete("/api/surveys/" + surveyId)
			.then(
					function(response) {
						for (var i = 0; i < $scope.surveys.length; i++) {
						    if ($scope.surveys[i].id == surveyId) {
						    	$scope.surveys.splice(i, 1);
						    	break;
						    }
						}
					}, 
					function(reason) {
						console.log(reason.data);
					}
			);
		};
	
		$scope.loadNextPage();
	};

	app.controller("mySurveysCtrl", ["$scope", "$http", mySurveysCtrl]);

}());