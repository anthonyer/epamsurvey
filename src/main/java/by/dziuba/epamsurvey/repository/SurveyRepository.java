package by.dziuba.epamsurvey.repository;

import by.dziuba.epamsurvey.model.Survey;
import by.dziuba.epamsurvey.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository("SurveyRepository")
public interface SurveyRepository extends PagingAndSortingRepository<Survey, Long> {

	Page<Survey> findByIsPublishedTrue(Pageable pageable);

	Page<Survey> findByCreatedBy(User user, Pageable pageable);
}
