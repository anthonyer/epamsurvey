package by.dziuba.epamsurvey.repository;

import by.dziuba.epamsurvey.model.Question;
import by.dziuba.epamsurvey.model.Survey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("QuestionRepository")
public interface QuestionRepository extends JpaRepository<Question, Long> {
	int countBySurvey(Survey survey);

	int countBySurveyAndValidityTrue(Survey survey);

	List<Question> findBySurveyOrderByOrderAsc(Survey survey);

	List<Question> findBySurveyAndValidityTrueOrderByOrderAsc(Survey survey);
}
