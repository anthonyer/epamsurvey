package by.dziuba.epamsurvey.service;

import by.dziuba.epamsurvey.exception.ActionRefusedException;
import by.dziuba.epamsurvey.exception.ResourceUnavailableException;
import by.dziuba.epamsurvey.model.Survey;
import by.dziuba.epamsurvey.model.User;
import by.dziuba.epamsurvey.repository.SurveyRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("SurveyService")
@Transactional
public class SurveyServiceImpl implements SurveyService {
	private static final Logger logger = LoggerFactory.getLogger(SurveyServiceImpl.class);

	private SurveyRepository surveyRepository;

	private QuestionService questionService;

	@Autowired
	public SurveyServiceImpl(SurveyRepository surveyRepository, QuestionService questionService) {
		this.surveyRepository = surveyRepository;
		this.questionService = questionService;
	}

	@Override
	public Survey save(Survey survey, User user) {
		survey.setCreatedBy(user);
		return surveyRepository.save(survey);
	}

	@Override
	public Page<Survey> findAll(Pageable pageable) {
		return surveyRepository.findAll(pageable);
	}

	@Override
	public Page<Survey> findAllPublished(Pageable pageable) {
		return surveyRepository.findByIsPublishedTrue(pageable);
	}

	@Override
	public Survey find(Long id) {
		Survey survey = surveyRepository.findOne(id);

		if (survey == null) {
			logger.error("Survey " + id + " not found");
			throw new ResourceUnavailableException("Survey " + id + " not found");
		}

		return survey;
	}

	@Override
	public Survey update(Survey newSurvey) {
		Survey currentSurvey = find(newSurvey.getId());

		mergeSurveys(currentSurvey, newSurvey);
		return surveyRepository.save(currentSurvey);
	}

	@Override
	public void delete(Survey survey) {
		surveyRepository.delete(survey);
	}

	@Override
	public Page<Survey> findSurveysByUser(User user, Pageable pageable) {
		return surveyRepository.findByCreatedBy(user, pageable);
	}

	@Override
	public void publishSurvey(Survey survey) {
		int count = questionService.countValidQuestionsInSurvey(survey);

		if (count > 0) {
			survey.setIsPublished(true);
			surveyRepository.save(survey);
		} else {
			throw new ActionRefusedException("The survey doesn't have any valid questions");
		}
	}

    private void mergeSurveys(Survey currentSurvey, Survey newSurvey) {
        currentSurvey.setName(newSurvey.getName());
        currentSurvey.setDescription(newSurvey.getDescription());
    }
}
