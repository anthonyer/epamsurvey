package by.dziuba.epamsurvey.service;

import by.dziuba.epamsurvey.model.Answer;
import by.dziuba.epamsurvey.model.Question;
import by.dziuba.epamsurvey.model.Survey;

import java.util.List;

public interface QuestionService {
	Question save(Question question);

	Question find(Long id);

	List<Question> findQuestionsBySurvey(Survey survey);

	List<Question> findValidQuestionsBySurvey(Survey survey);

	Question update(Question question);

	void delete(Question question);

	Answer addAnswerToQuestion(Answer answer, Question question);

	int countValidQuestionsInSurvey(Survey survey);
}
