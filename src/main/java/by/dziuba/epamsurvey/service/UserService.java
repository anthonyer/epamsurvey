package by.dziuba.epamsurvey.service;

import by.dziuba.epamsurvey.model.User;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {
	User saveUser(User user);

	User find(Long id);

	User findByEmail(String email);

	User findByUsername(String username);

	User setRegistrationCompleted(User user);

	boolean isRegistrationCompleted(User user);
}