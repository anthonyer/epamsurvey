package by.dziuba.epamsurvey.service.registration.util;

import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class TokenGeneratorUUID implements TokenGenerator {

	@Override
	public String generateRandomToken() {
		return UUID.randomUUID().toString();
	}
}
