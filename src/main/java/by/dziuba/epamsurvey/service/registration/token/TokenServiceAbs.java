package by.dziuba.epamsurvey.service.registration.token;

import by.dziuba.epamsurvey.exception.InvalidTokenException;
import by.dziuba.epamsurvey.model.TokenModel;
import by.dziuba.epamsurvey.model.User;
import by.dziuba.epamsurvey.repository.TokenRepository;
import by.dziuba.epamsurvey.service.registration.util.DateHelper;
import by.dziuba.epamsurvey.service.registration.util.TokenGenerator;
import org.springframework.beans.factory.annotation.Autowired;

import javax.transaction.Transactional;
import java.util.Date;

@Transactional
public abstract class TokenServiceAbs<T extends TokenModel> implements TokenService<T> {

	private TokenRepository<T> tokenRepo;
	private TokenGenerator tokenGenerator;

	@Autowired
	private DateHelper dateHelper;

	@Autowired
	public TokenServiceAbs(TokenGenerator tokenGenerator, TokenRepository<T> tokenRepo) {
		this.tokenRepo = tokenRepo;
		this.tokenGenerator = tokenGenerator;
	}

	@Override
	public T generateTokenForUser(User user) {
		T tokenModel = create();
		String token = tokenGenerator.generateRandomToken();

		tokenModel.setToken(token);
		tokenModel.setUser(user);
		tokenModel.setExpirationDate(
				dateHelper.getExpirationDate(dateHelper.getCurrentDate(), getExpirationTimeInMinutes()));

		return save(tokenModel);
	}

	@Override
	public void validateTokenForUser(User user, String token) {
		T tokenModel = findByToken(token);

		if (tokenModel == null) {
			throw new InvalidTokenException("Can't find token " + token);
		}

		if (!tokenModel.getUser().equals(user)) {
			throw new InvalidTokenException("Token " + token + " doesn't belong to user " + user.getId());
		}
	}

	@Override
	public void invalidateToken(String token) {
		T tokenModel = findByToken(token);
		delete(tokenModel);
	}

	@Override
	public void invalidateExpiredTokensPreviousTo(Date date) {
		tokenRepo.deletePreviousTo(date);
	}

	protected T save(T tokenModel) {
		return tokenRepo.save(tokenModel);
	}

	protected void delete(T tokenModel) {
		tokenRepo.delete(tokenModel);
	}

	private T findByToken(String token) {
		return tokenRepo.findByToken(token);
	}

	protected abstract T create();

	protected abstract int getExpirationTimeInMinutes();
}
