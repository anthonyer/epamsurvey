package by.dziuba.epamsurvey.service.registration.util;

public interface TokenGenerator {
	String generateRandomToken();
}