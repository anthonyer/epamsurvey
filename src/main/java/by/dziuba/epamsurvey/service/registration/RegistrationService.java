package by.dziuba.epamsurvey.service.registration;

import by.dziuba.epamsurvey.model.User;

public interface RegistrationService {
	User startRegistration(User user);

	User continueRegistration(User user, String token);

	boolean isRegistrationCompleted(User user);
}
