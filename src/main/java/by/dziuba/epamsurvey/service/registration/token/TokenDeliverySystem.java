package by.dziuba.epamsurvey.service.registration.token;

import by.dziuba.epamsurvey.model.TokenModel;
import by.dziuba.epamsurvey.model.User;
import org.springframework.scheduling.annotation.Async;

public interface TokenDeliverySystem {
	@Async
	void sendTokenToUser(TokenModel token, User user);
}
