package by.dziuba.epamsurvey.service.registration.token;

import by.dziuba.epamsurvey.model.RegistrationToken;
import by.dziuba.epamsurvey.repository.RegistrationTokenRepository;
import by.dziuba.epamsurvey.service.registration.util.TokenGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class TokenServiceRegistration extends TokenServiceAbs<RegistrationToken> {

	@Value("${epamSurvey.tokens.timeout}")
	private Integer expirationTimeInMinutes;

	@Autowired
	public TokenServiceRegistration(RegistrationTokenRepository mailTokenRepository, TokenGenerator tokenGenerator) {
		super(tokenGenerator, mailTokenRepository);
	}

	@Override
	protected RegistrationToken create() {
		return new RegistrationToken();
	}

	@Override
	protected int getExpirationTimeInMinutes() {
		return expirationTimeInMinutes;
	}
}
