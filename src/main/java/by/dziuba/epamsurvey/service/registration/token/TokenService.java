package by.dziuba.epamsurvey.service.registration.token;

import by.dziuba.epamsurvey.exception.InvalidTokenException;
import by.dziuba.epamsurvey.model.TokenModel;
import by.dziuba.epamsurvey.model.User;

import java.util.Date;

public interface TokenService<T extends TokenModel> {
	T generateTokenForUser(User user);

	void validateTokenForUser(User user, String token) throws InvalidTokenException;

	void invalidateToken(String token);

	void invalidateExpiredTokensPreviousTo(Date date);
}
