package by.dziuba.epamsurvey.service.registration.token;

import by.dziuba.epamsurvey.model.TokenModel;
import by.dziuba.epamsurvey.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class TokenDeliverySystemImpl implements TokenDeliverySystem {

	private static final String BASE_CONFIG_URI = "epamSurvey.tokens";

	private MessageSource messageSource;
	private JavaMailSender mailSender;

	@Autowired
	public TokenDeliverySystemImpl(MessageSource messageSource, JavaMailSender mailSender) {
		this.messageSource = messageSource;
		this.mailSender = mailSender;
	}

	@Override
	public void sendTokenToUser(TokenModel token, User user) {
		String url = String.format(messageSource.getMessage(BASE_CONFIG_URI + ".url", null, null), user.getId(),
				token.getToken());

		try {
			sendByMail(user, url, BASE_CONFIG_URI);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void sendByMail(User user, String url, String baseConfig) {
		String subject = messageSource.getMessage(baseConfig + ".subject", null, null);
		String body = String.format(messageSource.getMessage(baseConfig + ".body", null, null), user.getUsername(),
				url);

		SimpleMailMessage mailMessage = new SimpleMailMessage();

		mailMessage.setTo(user.getEmail());
		mailMessage.setFrom("noreply@epamsurvey.com");
		mailMessage.setSubject(subject);
		mailMessage.setText(body);

		mailSender.send(mailMessage);
	}
}
