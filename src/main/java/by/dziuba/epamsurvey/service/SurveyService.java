package by.dziuba.epamsurvey.service;

import by.dziuba.epamsurvey.model.Survey;
import by.dziuba.epamsurvey.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface SurveyService {
	Survey save(Survey survey, User user);

	Page<Survey> findAll(Pageable pageable);

	Page<Survey> findAllPublished(Pageable pageable);

	Page<Survey> findSurveysByUser(User user, Pageable pageable);

	Survey find(Long id);

	Survey update(Survey survey);

	void delete(Survey survey);

	void publishSurvey(Survey survey);
}
