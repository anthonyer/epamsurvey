package by.dziuba.epamsurvey.service;

import by.dziuba.epamsurvey.model.Answer;
import by.dziuba.epamsurvey.model.Question;

import java.util.List;

public interface AnswerService {
	Answer save(Answer answer);

	Answer find(Long id);

	Answer update(Answer newAnswer);

	void delete(Answer answer);

	List<Answer> findAnswersByQuestion(Question question);

	int countAnswersInQuestion(Question question);
}
