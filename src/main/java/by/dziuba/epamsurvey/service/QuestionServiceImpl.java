package by.dziuba.epamsurvey.service;

import by.dziuba.epamsurvey.exception.ActionRefusedException;
import by.dziuba.epamsurvey.exception.ResourceUnavailableException;
import by.dziuba.epamsurvey.model.Answer;
import by.dziuba.epamsurvey.model.Question;
import by.dziuba.epamsurvey.model.Survey;
import by.dziuba.epamsurvey.repository.QuestionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("QuestionService")
@Transactional
public class QuestionServiceImpl implements QuestionService {
	private static final Logger logger = LoggerFactory.getLogger(QuestionServiceImpl.class);

	private QuestionRepository questionRepository;

	private AnswerService answerService;

	@Autowired
	public QuestionServiceImpl(QuestionRepository questionRepository, AnswerService answerService) {
		this.questionRepository = questionRepository;
		this.answerService = answerService;
	}

	@Override
	public Question save(Question question) {
		int count = questionRepository.countBySurvey(question.getSurvey());
		question.setOrder(count + 1);

		return questionRepository.save(question);
	}

	@Override
	public Question find(Long id) {
		Question question = questionRepository.findOne(id);

		if (question == null) {
			logger.error("Question " + id + " not found");
			throw new ResourceUnavailableException("Question " + id + " not found");
		}

		return question;
	}

	@Override
	public Question update(Question newQuestion) {
		Question currentQuestion = find(newQuestion.getId());

		mergeQuestions(currentQuestion, newQuestion);
		return questionRepository.save(currentQuestion);
	}

	@Override
	public void delete(Question question) {
		Survey survey = question.getSurvey();

		if (survey.getIsPublished() && question.getValidity() && countValidQuestionsInSurvey(survey) <= 1) {
			throw new ActionRefusedException("A published Survey can't contain less than one valid question");
		}

		questionRepository.delete(question);
	}

	private void mergeQuestions(Question currentQuestion, Question newQuestion) {
		currentQuestion.setText(newQuestion.getText());

		if (newQuestion.getOrder() != null)
			currentQuestion.setOrder(newQuestion.getOrder());
	}

	@Override
	public List<Question> findQuestionsBySurvey(Survey survey) {
		return questionRepository.findBySurveyOrderByOrderAsc(survey);
	}

	@Override
	public List<Question> findValidQuestionsBySurvey(Survey survey) {
		return questionRepository.findBySurveyAndValidityTrueOrderByOrderAsc(survey);
	}

	@Override
	public Answer addAnswerToQuestion(Answer answer, Question question) {
		int count = answerService.countAnswersInQuestion(question);
		Answer newAnswer = updateAndSaveAnswer(answer, question, count);

        checkAndUpdateQuestionValidity(question);

		return newAnswer;
	}

	private Answer updateAndSaveAnswer(Answer answer, Question question, int count) {
		answer.setOrder(count + 1);
		answer.setQuestion(question);
		return answerService.save(answer);
	}

	private void checkAndUpdateQuestionValidity(Question question) {
		if (!question.getValidity()) {
			question.setValidity(true);
			save(question);
		}
	}

	@Override
	public int countValidQuestionsInSurvey(Survey survey) {
		return questionRepository.countBySurveyAndValidityTrue(survey);
	}
}
