package by.dziuba.epamsurvey.controller.web;

import by.dziuba.epamsurvey.controller.util.RestValidator;
import by.dziuba.epamsurvey.exception.ModelValidationException;
import by.dziuba.epamsurvey.model.AuthenticatedUser;
import by.dziuba.epamsurvey.model.Question;
import by.dziuba.epamsurvey.model.Survey;
import by.dziuba.epamsurvey.service.QuestionService;
import by.dziuba.epamsurvey.service.SurveyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class WebSurveyController {

	@Autowired
	SurveyService surveyService;

	@Autowired
	QuestionService questionService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home() {
		return "home";
	}

	@RequestMapping(value = "/createSurvey", method = RequestMethod.GET)
	@PreAuthorize("isAuthenticated()")
	public String newSurvey() {
		return "createSurvey";
	}

	@RequestMapping(value = "/createSurvey", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	public String newSurvey(@AuthenticationPrincipal AuthenticatedUser user, @Valid Survey survey, BindingResult result) {
		Survey newSurvey;

		try {
			RestValidator.verifyModelResult(result);
			newSurvey = surveyService.save(survey, user.getUser());
		} catch (ModelValidationException e) {
			return "createSurvey";
		}

		return "redirect:/editSurvey/" + newSurvey.getId();
	}

	@RequestMapping(value = "/editSurvey/{surveyId}", method = RequestMethod.GET)
	@PreAuthorize("isAuthenticated()")
	public ModelAndView editSurvey(@PathVariable long surveyId) {
		Survey survey = surveyService.find(surveyId);

		ModelAndView mav = new ModelAndView();
		mav.addObject("survey", survey);
		mav.setViewName("editSurvey");

		return mav;
	}

	@RequestMapping(value = "/editAnswer/{questionId}", method = RequestMethod.GET)
	@PreAuthorize("isAuthenticated()")
	public ModelAndView editAnswer(@PathVariable long questionId) {
		Question question = questionService.find(questionId);

		ModelAndView mav = new ModelAndView();
		mav.addObject("question", question);
		mav.setViewName("editAnswers");

		return mav;
	}

	@RequestMapping(value = "/survey/{surveyId}", method = RequestMethod.GET)
	@PreAuthorize("permitAll")
	public ModelAndView getSurvey(@PathVariable long surveyId) {
		Survey survey = surveyService.find(surveyId);

		ModelAndView mav = new ModelAndView();
		mav.addObject("survey", survey);
		mav.setViewName("surveyView");

		return mav;
	}

	@RequestMapping(value = "/survey/{surveyId}/take", method = RequestMethod.GET)
	@PreAuthorize("permitAll")
	public ModelAndView takeSurvey(@PathVariable long surveyId) {
		Survey survey = surveyService.find(surveyId);

		ModelAndView mav = new ModelAndView();
		mav.addObject("survey", survey);
		mav.setViewName("takeSurvey");

		return mav;
	}

    @RequestMapping(value = "/user/surveys", method = RequestMethod.GET)
    @PreAuthorize("isAuthenticated()")
    public String getSurveysForAuthenticatedUser() {
        return "mySurveys";
    }
}
