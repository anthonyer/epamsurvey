package by.dziuba.epamsurvey.controller.rest;

import by.dziuba.epamsurvey.controller.util.RestValidator;
import by.dziuba.epamsurvey.model.Answer;
import by.dziuba.epamsurvey.model.Question;
import by.dziuba.epamsurvey.service.AnswerService;
import by.dziuba.epamsurvey.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/answers")
public class AnswerController {
	@Autowired
	AnswerService answerService;

	@Autowired
	QuestionService questionService;

	@RequestMapping(value = "", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	@ResponseStatus(HttpStatus.CREATED)
	public Answer save(@Valid Answer answer, BindingResult result, @RequestParam long questionId) {
		RestValidator.verifyModelResult(result);

		Question question = questionService.find(questionId);
		return questionService.addAnswerToQuestion(answer, question);
	}

	@RequestMapping(value = "/updateAll", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	@ResponseStatus(HttpStatus.OK)
	public void updateAll(@RequestBody List<Answer> answers) {
		for (int i = 0; i < answers.size(); i++) {
			Answer answer = answers.get(i);
			answer.setOrder(i + 1);

			answerService.update(answer);
		}
	}

	@RequestMapping(value = "/{answerId}", method = RequestMethod.GET)
	@PreAuthorize("permitAll")
	@ResponseStatus(HttpStatus.OK)
	public Answer find(@PathVariable Long answerId) {
		return answerService.find(answerId);
	}

	@RequestMapping(value = "/{answerId}", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	@ResponseStatus(HttpStatus.OK)
	public Answer update(@PathVariable Long answerId, @Valid Answer answer, BindingResult result) {
		RestValidator.verifyModelResult(result);

		answer.setId(answerId);
		return answerService.update(answer);
	}

	@RequestMapping(value = "/{answerId}", method = RequestMethod.DELETE)
	@PreAuthorize("isAuthenticated()")
	@ResponseStatus(HttpStatus.OK)
	public void delete(@PathVariable Long answerId) {
		Answer answer = answerService.find(answerId);
		answerService.delete(answer);
	}
}
