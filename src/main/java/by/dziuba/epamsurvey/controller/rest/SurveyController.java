package by.dziuba.epamsurvey.controller.rest;

import by.dziuba.epamsurvey.controller.util.RestValidator;
import by.dziuba.epamsurvey.model.AuthenticatedUser;
import by.dziuba.epamsurvey.model.Question;
import by.dziuba.epamsurvey.model.Survey;
import by.dziuba.epamsurvey.model.User;
import by.dziuba.epamsurvey.service.QuestionService;
import by.dziuba.epamsurvey.service.SurveyService;
import by.dziuba.epamsurvey.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/surveys")
public class SurveyController {
	private static final Logger logger = LoggerFactory.getLogger(SurveyController.class);

	@Autowired
	private SurveyService surveyService;

	@Autowired
	private QuestionService questionService;

    @Autowired
    private UserService userService;

	@RequestMapping(value = "", method = RequestMethod.GET)
	@PreAuthorize("permitAll")
	@ResponseStatus(HttpStatus.OK)
	public Page<Survey> findAll(Pageable pageable,
								@RequestParam(required = false, defaultValue = "false") Boolean published) {
		if (published) {
			return surveyService.findAllPublished(pageable);
		} else {
			return surveyService.findAll(pageable);
		}
	}

    @RequestMapping(value = "/{userId}/surveys", method = RequestMethod.GET)
    @PreAuthorize("permitAll")
    @ResponseStatus(HttpStatus.OK)
    public Page<Survey> getSurveysByUser(Pageable pageable, @PathVariable Long userId) {
        logger.debug("Requested page " + pageable.getPageNumber() + " from user " + userId);

        User user = userService.find(userId);
        return surveyService.findSurveysByUser(user, pageable);
    }

	@RequestMapping(value = "", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	@ResponseStatus(HttpStatus.CREATED)
	public Survey save(@AuthenticationPrincipal AuthenticatedUser user, @Valid Survey survey, BindingResult result) {
		logger.debug("The Survey " + survey.getName() + " is going to be created");
		
		RestValidator.verifyModelResult(result);

		return surveyService.save(survey, user.getUser());
	}

	@RequestMapping(value = "/{surveyId}", method = RequestMethod.GET)
	@PreAuthorize("permitAll")
	@ResponseStatus(HttpStatus.OK)
	public Survey find(@PathVariable Long surveyId) {
		return surveyService.find(surveyId);
	}

	@RequestMapping(value = "/{surveyId}", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	@ResponseStatus(HttpStatus.OK)
	public Survey update(@PathVariable Long surveyId, @Valid Survey survey, BindingResult result) {
		RestValidator.verifyModelResult(result);

		survey.setId(surveyId);
		return surveyService.update(survey);
	}

	@RequestMapping(value = "/{surveyId}", method = RequestMethod.DELETE)
	@PreAuthorize("isAuthenticated()")
	@ResponseStatus(HttpStatus.OK)
	public void delete(@PathVariable Long surveyId) {
		Survey survey = surveyService.find(surveyId);
		surveyService.delete(survey);
	}

	@RequestMapping(value = "/{surveyId}/questions", method = RequestMethod.GET)
	@PreAuthorize("permitAll")
	@ResponseStatus(HttpStatus.OK)
	public List<Question> findQuestions(@PathVariable Long surveyId,
			@RequestParam(required = false, defaultValue = "false") Boolean onlyValid) {
		Survey survey = surveyService.find(surveyId);

		if (onlyValid) {
			return questionService.findValidQuestionsBySurvey(survey);
		} else {
			return questionService.findQuestionsBySurvey(survey);
		}
	}

	@RequestMapping(value = "/{surveyId}/publish", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	@ResponseStatus(HttpStatus.OK)
	public void publishSurvey(@PathVariable long surveyId) {
		Survey survey = surveyService.find(surveyId);
		surveyService.publishSurvey(survey);
	}
}
