package by.dziuba.epamsurvey.controller.rest;

import by.dziuba.epamsurvey.controller.util.RestValidator;
import by.dziuba.epamsurvey.model.Answer;
import by.dziuba.epamsurvey.model.Question;
import by.dziuba.epamsurvey.model.Survey;
import by.dziuba.epamsurvey.service.AnswerService;
import by.dziuba.epamsurvey.service.QuestionService;
import by.dziuba.epamsurvey.service.SurveyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/questions")
public class QuestionController {
	@Autowired
	private QuestionService questionService;

	@Autowired
	private SurveyService surveyService;

	@Autowired
	private AnswerService answerService;

	@RequestMapping(value = "", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	@ResponseStatus(HttpStatus.CREATED)
	public Question save(@Valid Question question, BindingResult result, @RequestParam Long surveyId) {
		RestValidator.verifyModelResult(result);

		Survey survey = surveyService.find(surveyId);
		question.setSurvey(survey);

		return questionService.save(question);
	}

	@RequestMapping(value = "/updateAll", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	@ResponseStatus(HttpStatus.OK)
	public void updateAll(@RequestBody List<Question> questions) {
		for (int i = 0; i < questions.size(); i++) {
			Question question = questions.get(i);
			question.setOrder(i + 1);

			questionService.update(question);
		}
	}

	@RequestMapping(value = "/{questionId}", method = RequestMethod.GET)
	@PreAuthorize("permitAll")
	@ResponseStatus(HttpStatus.OK)
	public Question find(@PathVariable Long questionId) {
		return questionService.find(questionId);
	}

	@RequestMapping(value = "/{questionId}", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated()")
	@ResponseStatus(HttpStatus.OK)
	public Question update(@PathVariable Long questionId, @Valid Question question, BindingResult result) {
		RestValidator.verifyModelResult(result);

		question.setId(questionId);
		return questionService.update(question);
	}

	@RequestMapping(value = "/{questionId}", method = RequestMethod.DELETE)
	@PreAuthorize("isAuthenticated()")
	@ResponseStatus(HttpStatus.OK)
	public void delete(@PathVariable Long questionId) {
		Question question = questionService.find(questionId);
		questionService.delete(question);
	}

	@RequestMapping(value = "/{questionId}/answers", method = RequestMethod.GET)
	@PreAuthorize("permitAll")
	@ResponseStatus(HttpStatus.OK)
	public List<Answer> findAnswers(@PathVariable Long questionId) {
		Question question = questionService.find(questionId);
		return answerService.findAnswersByQuestion(question);
	}
}
