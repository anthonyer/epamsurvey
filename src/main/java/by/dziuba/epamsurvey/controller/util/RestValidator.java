package by.dziuba.epamsurvey.controller.util;

import by.dziuba.epamsurvey.exception.ModelValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.BindingResult;

public class RestValidator {
	private static final Logger logger = LoggerFactory.getLogger(RestValidator.class);

	public static void verifyModelResult(BindingResult result) {
		if (result.hasErrors()) {
			logger.error(result.toString());
			throw new ModelValidationException(result.getFieldError().getDefaultMessage());
		}
	}
}
