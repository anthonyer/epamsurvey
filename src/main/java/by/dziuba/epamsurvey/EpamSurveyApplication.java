package by.dziuba.epamsurvey;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan(basePackages = "by.dziuba.epamsurvey")
public class EpamSurveyApplication {
	public static void main(String[] args) {
		SpringApplication.run(EpamSurveyApplication.class, args);
	}
}
