package by.dziuba.epamsurvey.exception;

public class ModelValidationException extends EpamSurveyException {

	private static final long serialVersionUID = 1L;

    public ModelValidationException(String message) {
		super(message);
	}
}
