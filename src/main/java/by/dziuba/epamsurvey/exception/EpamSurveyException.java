package by.dziuba.epamsurvey.exception;

class EpamSurveyException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	EpamSurveyException() {
		super();
	}

	EpamSurveyException(String message) {
		super(message);
	}
}
