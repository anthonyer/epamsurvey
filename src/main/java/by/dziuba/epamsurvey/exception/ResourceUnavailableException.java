package by.dziuba.epamsurvey.exception;

public class ResourceUnavailableException extends EpamSurveyException {

	private static final long serialVersionUID = 1L;

    public ResourceUnavailableException(String message) {
		super(message);
	}
}
