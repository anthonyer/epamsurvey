package by.dziuba.epamsurvey.exception;

public class InvalidTokenException extends EpamSurveyException {

	private static final long serialVersionUID = 1L;

    public InvalidTokenException(String message) {
		super(message);
	}
}