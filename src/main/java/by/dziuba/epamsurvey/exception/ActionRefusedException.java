package by.dziuba.epamsurvey.exception;

public class ActionRefusedException extends EpamSurveyException {

	private static final long serialVersionUID = 1L;

    public ActionRefusedException(String message) {
		super(message);
	}
}