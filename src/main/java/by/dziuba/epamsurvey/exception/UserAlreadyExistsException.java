package by.dziuba.epamsurvey.exception;

public class UserAlreadyExistsException extends EpamSurveyException {

	private static final long serialVersionUID = 1L;

    public UserAlreadyExistsException(String message) {
		super(message);
	}
}
